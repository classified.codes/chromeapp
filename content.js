(function() {
    function getCredentials() {
        return new Promise((resolve, reject) => {
            chrome.runtime.sendMessage({
                type: 'get_credentials',
                data: window.location.hostname
            }, response => {
                resolve(response);
            });
        });
    }

    getCredentials()
    .then(response => {
        console.log(response);
    });
})();
